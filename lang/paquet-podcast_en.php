<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-podcast?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'podcast_description' => 'A plugin to provide one or several podcasts feeds on the site',
	'podcast_nom' => 'Podcasts',
	'podcast_slogan' => 'One or several podcasts feeds on the site'
);
