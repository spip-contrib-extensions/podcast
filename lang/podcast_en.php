<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/podcast?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_podcast' => 'Podcasts',

	// D
	'descriptif_flux_defaut' => '@site@ - Recents medias',

	// E
	'explication_copyright' => 'A sentence explaining the rights of the podcast feed.',
	'explication_description' => 'Description of the podcast feed. If this field is not filled, the description of the site will be used. Otherwise a generic sentence is used.',
	'explication_email_auteur' => 'Generic email address of the author / owner of the podcast. If this field is left blank, the email of the webmaster of the site will be used.',
	'explication_explicit' => 'Default value for the "explicit" value in the podcast feed for each item.',
	'explication_itunes_categories_principales' => 'These categories allow you to classify feeds into the Apple Store. Some of them may contain sub-categories.',
	'explication_keywords' => 'Keywords associated with the feed. Limit to 12 and separate them by commas.',
	'explication_nom_auteur' => 'Generic name of the author and owner of the podcast. Useful for its referencing.',
	'explication_podcast_auto' => 'Documents added to the site are automatically included in the podcast feed (It is possible to delete or add them later).',
	'explication_podcast_types' => 'What types of feeds are available to visitors.',
	'explication_resume' => 'Short summary of the content of the podcast feed. If this field is not filled, the slogan of the site will be used. If the latter is not available, a default sentence take their place.',
	'explication_titre' => 'If this field is left blank, the name of the site will be used.',

	// I
	'itunes_cat_alternative_health' => 'Alternative Health',
	'itunes_cat_amateur' => 'Amateur',
	'itunes_cat_arts' => 'Arts',
	'itunes_cat_automotive' => 'Automotive',
	'itunes_cat_aviation' => 'Aviation',
	'itunes_cat_buddhism' => 'Buddhism',
	'itunes_cat_business' => 'Business',
	'itunes_cat_business_news' => 'Business News',
	'itunes_cat_business_shopping' => 'Shopping',
	'itunes_cat_careers' => 'Careers',
	'itunes_cat_christianity' => 'Christianity',
	'itunes_cat_college_high_school' => 'College & High School',
	'itunes_cat_comedy' => 'Comedy',
	'itunes_cat_design' => 'Design',
	'itunes_cat_education' => 'Education',
	'itunes_cat_education_technology' => 'Education Technology',
	'itunes_cat_fashion_beauty' => 'Fashion & Beauty',
	'itunes_cat_fitness_nutrition' => 'Fitness & Nutrition',
	'itunes_cat_food' => 'Food',
	'itunes_cat_gadgets' => 'Gadgets',
	'itunes_cat_games_hobbies' => 'Games & Hobbies',
	'itunes_cat_government_organizations' => 'Government & Organizations',
	'itunes_cat_health' => 'Health',
	'itunes_cat_higher_education' => 'Higher Education',
	'itunes_cat_hinduism' => 'Hinduism',
	'itunes_cat_history' => 'History',
	'itunes_cat_hobbies' => 'Hobbies',
	'itunes_cat_investing' => 'Investing',
	'itunes_cat_islam' => 'Islam',
	'itunes_cat_judaism' => 'Judaism',
	'itunes_cat_k_12' => 'K-12',
	'itunes_cat_kids_family' => 'Kids & Family',
	'itunes_cat_language_courses' => 'Language Courses',
	'itunes_cat_literature' => 'Literature',
	'itunes_cat_local' => 'Local',
	'itunes_cat_management_marketting' => 'Management & Marketing',
	'itunes_cat_medicine' => 'Medicine',
	'itunes_cat_music' => 'Music',
	'itunes_cat_national' => 'National',
	'itunes_cat_natural_sciences' => 'Natural Sciences',
	'itunes_cat_news_politics' => 'News & Politics',
	'itunes_cat_non_profit' => 'Non-Profit',
	'itunes_cat_other' => 'Other',
	'itunes_cat_other_games' => 'Other Games',
	'itunes_cat_outdoor' => 'Outdoor',
	'itunes_cat_performing_arts' => 'Performing Arts',
	'itunes_cat_personnal_journals' => 'Personal Journals',
	'itunes_cat_philosophy' => 'Philosophy',
	'itunes_cat_places_travel' => 'Places & Travel',
	'itunes_cat_podcasting' => 'Podcasting',
	'itunes_cat_professional' => 'Professional',
	'itunes_cat_regional' => 'Regional',
	'itunes_cat_religion_spirituality' => 'Religion & Spirituality',
	'itunes_cat_science_medicine' => 'Science & Medicine',
	'itunes_cat_self_help' => 'Self-Help',
	'itunes_cat_sexuality' => 'Sexuality',
	'itunes_cat_social_sciences' => 'Social Sciences',
	'itunes_cat_society_culture' => 'Society & Culture',
	'itunes_cat_software_how_to' => 'Software How-To',
	'itunes_cat_spirituality' => 'Spirituality',
	'itunes_cat_sports_recreation' => 'Sports & Recreation',
	'itunes_cat_tech_news' => 'Tech News',
	'itunes_cat_technology' => 'Technology',
	'itunes_cat_training' => 'Training',
	'itunes_cat_tv_film' => 'TV & Film',
	'itunes_cat_video_games' => 'Video Games',
	'itunes_cat_visual_arts' => 'Visual Arts',

	// L
	'label_contenu_explicit' => 'Explicit content',
	'label_copyright' => 'Feed copyright',
	'label_dans_podcast' => 'In the podcast feed',
	'label_description' => 'Description of the podcast feed',
	'label_email_auteur' => 'Author’s email / owner',
	'label_explicit' => 'Explicit content',
	'label_itunes_categories_principales' => 'Main categories for iTunes',
	'label_itunes_sous_categories' => 'Subcategories of "@cat@"',
	'label_keywords' => 'Keywords',
	'label_nom_auteur' => 'Author’s name / owner',
	'label_podcast_auto' => 'Automatic podcast',
	'label_podcast_types' => 'Feeds types enabled',
	'label_resume' => 'Summary of the podcast feed',
	'label_titre' => 'Title of the podcast feed',
	'legend_itunes' => 'Specific configuration for iTunes',
	'legend_mrss' => 'Specific configuration for Media RSS',

	// V
	'valeur_clean' => 'clean',
	'valeur_itunes' => 'iTunes',
	'valeur_miro' => 'Miro',
	'valeur_mrss' => 'Media RSS',
	'valeur_no' => 'No',
	'valeur_yes' => 'Yes'
);
