<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/podcast.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'podcast_description' => 'Un plugin pour mettre à disposition un ou plusieurs flux de podcast sur son site',
	'podcast_nom' => 'Podcasts',
	'podcast_slogan' => 'Un ou plusieurs flux de podcast sur son site'
);
